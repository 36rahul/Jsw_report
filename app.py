from flask import Flask,request,render_template,jsonify
import MySQLdb as mdb
app = Flask(__name__)

class MainObject():
        def __init__(self, app):
		self.app = app
		
		@self.app.route('/')
		@app.route('/index')
		def index():
                        """Reurn index.html file from templates folder"""
			return render_template('index.html')
       
		@app.route('/about')
		def about():
                        """Reurn index.html file from templates folder"""
			return render_template('about.html')

		def make_data(serial_no):
                        """Create data required for graph generation from the database in dictionary format"""
                        db = mdb.connect("localhost","root","gnulinux","width_measure")
                        cursor = db.cursor()
                        sql = "select primaryKey,refWidth,tolLower,tolHigher from slNoTable where slNo=%d" %(serial_no,)
                        cursor.execute(sql)
                        value = cursor.fetchall()
                        try:
                                index = int(value[0][0])
                                width_limit = value[0][1]
                                lower_limit = width_limit - value[0][2]
                                higher_limit = width_limit + value[0][3]
                        except:
                                return "Serial Number Not exist"
                        sql = "select lengthAtWhichMeasurementMade,measuredWidth from dataTable where slNoRefFromSlNoTable=%d" %(index)
                        cursor.execute(sql)
                        data = cursor.fetchall()
                        try:
                                graph_data = [list(elem) for elem in data]
                                limit_dict = {'lower_limit':int(lower_limit),'serial_no':serial_no,'higher_limit':int(higher_limit),'data':graph_data}
                                return limit_dict
                        except:
                                return "Sheet not measured yet"
                                
		@app.route('/graph', methods=['POST'])
		def graph_generator():
                        """Return the values to the index.html file as a ajax request"""
			sheetName = int(request.form['serial_no'])
			graph_data = make_data(sheetName)
			return jsonify(name = graph_data)

if __name__ == '__main__':
    mainObject = MainObject(app)
    mainObject.app.run(host='0.0.0.0') 
